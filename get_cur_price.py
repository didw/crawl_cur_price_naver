import urllib
import time
from threading import Thread 
from urllib.request import urlopen
from bs4 import BeautifulSoup
from openpyxl import Workbook
import json
import codecs
from tqdm import tqdm
import glob
import pandas as pd


code_table = {}
MARKET_CODE_DICT = {
    'kospi': 'stockMkt',
    'kosdaq': 'kosdaqMkt',
    'konex': 'konexMkt'
}

DOWNLOAD_URL = 'kind.krx.co.kr/corpgeneral/corpList.do'

def download_stock_codes(market=None, delisted=False):
  params = {'method': 'download'}

  if market.lower() in MARKET_CODE_DICT:
      params['marketType'] = MARKET_CODE_DICT[market]

  if not delisted:
      params['searchType'] = 13

  params_string = urllib.parse.urlencode(params)
  request_url = urllib.parse.urlunsplit(['http', DOWNLOAD_URL, '', params_string, ''])
  print(request_url)

  df = pd.read_html(request_url, header=0)[0]
  for i,row in df.iterrows():
    key = '{:06}'.format(row['종목코드'])
    value = row['회사명']
    code_table[key] = value

def read_code_table():
  download_stock_codes('kospi')
  download_stock_codes('kosdaq')
  download_stock_codes('konex')

def get_cur_price(stock_code):
  stock_code = stock_code.strip()
  page = 1
  url = 'http://finance.naver.com/item/sise_day.nhn?code=' + stock_code +'&page='+ str(page)
  html = urlopen(url)
  source = BeautifulSoup(html.read(), "html.parser")
  srlists=source.find_all("tr")
  idx = 2
  cur_price = srlists[idx].find_all("td",class_="num")[0].text
  try:
    stockName = code_table[stock_code]
  except KeyError:
    stockName = 'noname'
  with open('result/%s.txt'%stock_code, 'w') as f:
    f.write("%s\t%s\n"%(stockName, cur_price))


def join_result():
  wb = Workbook()
  ws = wb.active
  ws['A1'] = '종목명'
  ws['B1'] = '종목코드'
  ws['C1'] = '현재가'
  stock_list = glob.glob('result/*.txt')
  for r, stockfile in enumerate(stock_list):
    stockItem = stockfile.split('\\')[1].split('.')[0]
    with open(stockfile) as f:
      stockName, cur_price = f.readline().split('\t')
      ws['A%d'%(r+2)] = stockName
      ws['B%d'%(r+2)] = stockItem
      ws['C%d'%(r+2)] = cur_price
  wb.save('stock_cur_price.xlsx')


def main():
  print("read code table")
  read_code_table()
  print("Done, len(code_table):%d"%len(code_table))
  threads = []
  print("crawling current price")
  with open('stock_list.txt') as f:
    stock_list = f.readlines()
    for stock_code in tqdm(stock_list, desc='make thread', ncols=100):
      t = Thread(target=get_cur_price, args=(stock_code,))
      t.daemon = True
      threads.append(t)
    for t in tqdm(threads, desc='start thread', ncols=100):
      t.start()
    for t in tqdm(threads, desc='wait finish', ncols=100):
      t.join()
  print("save excel to stock_cur_price.xlsx")
  join_result()
  print("Done")


if __name__ == '__main__':
  main()
